export const groupIcons = {
  "Health": "health",
  "Heritage & Demographics": "demographics",
  "Housing": "housing",
  "Labor": "workforce",
  "Education": "education",
  "Government": "operations"
};

export const examples = [

  /* Heritage & Demographics */

  {
    title: "Foreign-Born Citizens by State",
    group: "Heritage & Demographics",
    image: "/api/profile/cip/regional-studies-us-canadian-foreign/thumb",
    link: "/visualize?groups=0-TBhjH&measure=64auG"
  },
  {
    title: "Russian Speakers by State",
    group: "Heritage & Demographics",
    image: "/images/profile/russia.jpg",
    link: "/visualize?groups=0-yiXGF&groups=1-1ac173-9&measure=ZJtXqi"
  },
  {
    title: "Household Income by Race",
    group: "Heritage & Demographics",
    image: "/api/profile/cip/public-finance/thumb",
    link: "/visualize?groups=0-Z169Wxp&measure=Zn7xp5"
  },
  {
    title: "Poverty Rate by Race",
    group: "Heritage & Demographics",
    image: "/api/profile/cip/demography-population-studies-6/thumb",
    link: "/visualize?groups=0-28NmKu&measure=AFRAP"
  },
  {
    title: "Average Age by State",
    group: "Heritage & Demographics",
    image: "/api/profile/soc/childcare-workers/thumb",
    link: "/visualize?groups=0-z9TnC&measure=1Kkgfr"
  },

  /* Health */

  {
    title: "Opioid Deaths by County",
    group: "Health",
    image: "/api/profile/cip/pharmacology/thumb",
    link: "/visualize?groups=0-QuaqK&measure=2sUCF4"
  },
  {
    title: "Adult Obesity by County",
    group: "Health",
    image: "/api/profile/cip/190505/thumb",
    link: "/visualize?groups=0-Z1X72Pg&measure=Z1iORxu"
  },
  {
    title: "Food Environment Index",
    group: "Health",
    image: "/api/profile/cip/120507/thumb",
    link: "/visualize?groups=0-Z1X72Pg&measure=Z1xNbvc"
  },
  {
    title: "Adults with Major Depressive Disorder by State",
    group: "Health",
    image: "/api/profile/cip/420101/thumb",
    link: "/visualize?groups=0-Zc58ag&measure=3DN30"
  },
  {
    title: "Adults who Cannot Afford a Doctor",
    group: "Health",
    image: "/api/profile/naics/6211/thumb",
    link: "/visualize?groups=0-14F1wI&measure=1wObYo"
  },

  /* Housing */

  {
    title: "Home Ownership by State",
    group: "Housing",
    image: "/api/profile/naics/531/thumb",
    link: "/visualize?groups=0-Z4LzeD&measure=Z1X8D4i"
  },
  {
    title: "Average Commute Time by Metro Area",
    group: "Housing",
    image: "/api/profile/soc/474051/thumb",
    link: "/visualize?groups=0-ZNTHUM&measure=ZMlbux"
  },
  {
    title: "People Driving Alone to Work by County",
    group: "Housing",
    image: "/api/profile/soc/5360XX/thumb",
    link: "/visualize?groups=0-Z1X72Pg&measure=2bT8FH"
  },
  {
    title: "Renter Occupied Households by State",
    group: "Housing",
    image: "/api/profile/naics/4232/thumb",
    link: "/visualize?groups=0-Z4LzeD&groups=1-Z1xMhVU-1&measure=Z1X8D4i"
  },
  {
    title: "Households with Internet Access by State",
    group: "Housing",
    image: "/api/profile/cip/computer-science-6/thumb",
    link: "/visualize?groups=0-49LjT&groups=1-Z2dwtr2-0~1&measure=fG9Wy"
  },

  /* Labor */

  {
    title: "Black Females working in the Software Industry by State",
    group: "Labor",
    image: "/api/profile/soc/151131/thumb",
    link: "/visualize?groups=0-z9TnC&groups=1-Z1Oby8M-2&groups=2-1mjmRl-5112&groups=3-1dQe8s-2&measure=1qWfo"
  },
  {
    title: "German-Borns Working in the Performing Arts Industry",
    group: "Labor",
    image: "/api/profile/cip/509999/thumb",
    link: "/visualize?groups=0-z9TnC&groups=1-1SyFhe-110&groups=2-1mjmRl-711&measure=1qWfo"
  },
  {
    title: "Naturalized US Citizens Working as Computer Scientists and Web Developers",
    group: "Labor",
    image: "/api/profile/cip/general-computer-programming/thumb",
    link: "/visualize?groups=0-z9TnC&groups=1-13xVLW-4&groups=2-19hV1x-151111~151131~151134~15113X&measure=1qWfo"
  },
  {
    title: "Median Earnings in the Construction Industry by State",
    group: "Labor",
    image: "/api/profile/cip/46/thumb",
    link: "/visualize?groups=0-Z1DVCsC&groups=1-Z1vDbOy-2&measure=ALgX7"
  },
  {
    title: "Income Inequality by Metro Area",
    group: "Labor",
    image: "/api/profile/napcs/41/thumb",
    link: "/visualize?groups=0-23eSQ7&measure=230vWl"
  },
  // {
  //   title: "Coal Mining Workers by State",
  //   group: "Labor",
  //   image: "/api/profile/naics/mining-quarrying-oil-gas-extraction/thumb",
  //   link: "/visualize?filters=0-Z2nLcvC-5-5&filters=1-1qWfo-4-2000&groups=0-z9TnC&groups=1-1mjmRl-2121&measure=1qWfo"
  // },

  /* Education */

  {
    title: "Admissions for Universities in the Boston Metro Area",
    group: "Education",
    image: "/api/profile/geo/boston-cambridge-quincy-ma-nh-metro-area/thumb",
    link: "/visualize?groups=0-1CdfJW&groups=1-1YEWx6-31000US14460&measure=Z1GSog1"
  },
  {
    title: "Default Rate by State",
    group: "Education",
    image: "/api/profile/soc/434131/thumb",
    link: "/visualize?groups=0-24s1dP&measure=ZdVPNx"
  },
  {
    title: "Engineering Graduates by Gender",
    group: "Education",
    image: "/api/profile/cip/14/thumb",
    link: "/visualize?groups=0-BNDBG&groups=1-Z2i3TId-14&measure=1g4zN3"
  },
  {
    title: "Average Number of Off-Campus Students by State",
    group: "Education",
    image: "/api/profile/cip/railroad-railway-transportation/thumb",
    link: "/visualize?groups=0-JrDjD-Off+campus&groups=1-Z1EiMXs&measure=Z7tI0u"
  },
  {
    title: "Benefit Expenditure by University Group",
    group: "Education",
    image: "/api/profile/soc/healthcare-support-workers-all-other-including-medical-equipment-preparers/thumb",
    link: "/visualize?groups=0-1XM2Wg&measure=zY3EL"
  },

  /* Government */

  {
    title: "Department of Defense Spending by State",
    group: "Government",
    image: "/api/profile/soc/550000/thumb",
    link: "/visualize?groups=0-Z1MxM8L&groups=1-1pz0Cl-97&measure=1e64mv"
  },
  {
    title: "National Science Foundations Grants by State",
    group: "Government",
    image: "/api/profile/soc/1930XX/thumb",
    link: "/visualize?groups=0-Z1MxM8L&groups=1-1pz0Cl-49&groups=2-Z27NDkh-Grant&measure=1e64mv"
  },
  {
    title: "Department of Interior Spending by State",
    group: "Government",
    image: "/api/profile/geo/washington-dc/thumb",
    link: "/visualize?groups=0-Z1MxM8L&groups=1-1pz0Cl-14&measure=1e64mv"
  },
  {
    title: "World War II Veterans by State",
    group: "Government",
    image: "/api/profile/naics/active-duty-military/thumb",
    link: "/visualize?groups=0-16Tk7O&groups=1-Z226eaa-4&measure=Z1LTgi2"
  },
  {
    title: "Public Administration Workforce by Race",
    group: "Government",
    image: "/api/profile/naics/public-administration/thumb",
    link: "/visualize?groups=0-23ay2s-92&groups=1-1dQe8s&measure=1qWfo"
  }
];
